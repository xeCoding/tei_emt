package com.example.aris.tei_emt.Adapters.LessonRecycler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.aris.tei_emt.Activities.LectureActivity;
import com.example.aris.tei_emt.Model.LessonDto;
import com.example.aris.tei_emt.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<LessonViewHolder> {

    private Context activityContext;
    private List<LessonDto> dataList;

    public RecyclerAdapter(Context context, List<LessonDto> dataList) {
        this.activityContext = context;
        this.dataList = new ArrayList<>();
        this.dataList.addAll(dataList);
    }


    /**
     *  Edo dilonete to ti layout tha exei to trexon stoixeio pou prokeitai na dimiourgithi
     *
     */
    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activityContext)
                .inflate(R.layout.lesson_item, parent, false);

        return new LessonViewHolder(view);
    }

    /**
     *  Otan exei dimiourgithei to view item kanoume match/antikatastasi tou kathe pediou apo to view
     *  me ta antistixa dedomena tou trexodos stoixeiou tou data
     */
    @Override
    public void onBindViewHolder(@NonNull final LessonViewHolder holder, final int position) {
        // ToDo setup Card Fields
        holder.courseName.setText(dataList.get(position).title);
        holder.courseCode.setText(dataList.get(position).code);
        holder.courseSemester.setText(String.format("%s Εξάμηνο", dataList.get(position).semester));
        holder.teamCode.setText(dataList.get(position).lsNo);
        holder.courseDay.setText(dataList.get(position).wdname);
        holder.courseTime.setText(dataList.get(position).fullTime);


        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activityContext, LectureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("lectureSectionDto", dataList.get(holder.getAdapterPosition()));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtras(bundle);
                activityContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
