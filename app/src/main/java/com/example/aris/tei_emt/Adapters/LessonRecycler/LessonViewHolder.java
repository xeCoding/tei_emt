package com.example.aris.tei_emt.Adapters.LessonRecycler;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.aris.tei_emt.R;

public class LessonViewHolder extends RecyclerView.ViewHolder {

    TextView courseName;
    TextView courseCode;
    TextView courseSemester;
    TextView teamCode;
    TextView courseDay;
    TextView courseTime;
    View view;

    public LessonViewHolder(View itemView) {
        super(itemView);

        courseName = itemView.findViewById(R.id.courseName);
        courseCode = itemView.findViewById(R.id.studentName);
        courseSemester = itemView.findViewById(R.id.courseSemester);
        teamCode = itemView.findViewById(R.id.teamCode);
        courseDay = itemView.findViewById(R.id.courseDay);
        courseTime = itemView.findViewById(R.id.courseTime);
        view = itemView;
    }

}
