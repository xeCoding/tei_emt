package com.example.aris.tei_emt.Activities;

import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.example.aris.tei_emt.Utilities.Parameters;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.aris.tei_emt.Fragments.AcademicFragment;
import com.example.aris.tei_emt.Fragments.CoursesFragment;
import com.example.aris.tei_emt.Fragments.StudentFragment;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.SystemUtils;

import java.sql.SQLOutput;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        CoursesFragment.OnFragmentInteractionListener,
        StudentFragment.OnFragmentInteractionListener,
        AcademicFragment.OnFragmentInteractionListener {

    private int drawerCheckedId = R.id.nav_courses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction().replace(R.id.content_main, new CoursesFragment());
        fragmentTransaction.commit();

        getSupportActionBar().setTitle("Δηλωμένα Μαθήματα");

        TextView nav_name = navigationView.getHeaderView(0).findViewById(R.id.nav_name);
        TextView nav_email = navigationView.getHeaderView(0).findViewById(R.id.nav_email);

        nav_name.setText(String.format("%s %s", Parameters.student.fname, Parameters.student.lname));
        nav_email.setText(String.format("%s", Parameters.student.email));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        Fragment fragment = null;

        //ToDo change setTitle onFragmentChoose

        if (id == R.id.nav_courses) {
            fragment = new CoursesFragment();
            getSupportActionBar().setTitle("Δηλωμένα Μαθήματα");

        } else if (id == R.id.nav_student) {
            fragment = new StudentFragment();
            getSupportActionBar().setTitle("Στοιχεία Φοιτητή");

        } else if (id == R.id.nav_academic) {
            fragment = new AcademicFragment();
            getSupportActionBar().setTitle("Ακαδημαϊκή Περίοδος");

        } else if (id == R.id.action_logout) {
            SystemUtils.logoutUser();
            return true;

        } if (id == R.id.action_unregister) {
            SystemUtils.unregisterDevice();
            return true;
        }

        if (fragment != null) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                    .beginTransaction().replace(R.id.content_main, fragment).addToBackStack(fragment.getClass().getName());
            fragmentTransaction.commit();
            drawerCheckedId = id;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
