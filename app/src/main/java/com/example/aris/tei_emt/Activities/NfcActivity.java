package com.example.aris.tei_emt.Activities;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aris.tei_emt.R;

public class NfcActivity extends AppCompatActivity {

    private TextView nfcText;
    private NfcAdapter mNfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nfc);

        if (getSupportActionBar() != null)
            setTitle("Nfc Παρουσία");

        nfcText = findViewById(R.id.nfcTextView);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (mNfcAdapter == null) {
            // Stop here, we definitely need NFC
            Toast.makeText(this, "This device doesn't support NFC.", Toast.LENGTH_LONG).show();
            finish();
            return;

        }

        if (!mNfcAdapter.isEnabled()) {
            nfcText.setText("NFC is disabled.");
        } else {
            nfcText.setText("NFC is enabled");
        }

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        // TODO: handle Intent
    }
}
