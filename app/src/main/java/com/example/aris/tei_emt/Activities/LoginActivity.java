package com.example.aris.tei_emt.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.aris.tei_emt.Model.RegisterDto;
import com.example.aris.tei_emt.Model.UserDto;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.Parameters;
import com.example.aris.tei_emt.Utilities.ParserFactory;
import com.example.aris.tei_emt.Utilities.Requests;
import com.example.aris.tei_emt.Utilities.SystemUtils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;


public class LoginActivity extends AppCompatActivity {

    private EditText usernameTextView;
    private EditText passwordTextView;
    private ProgressDialog loginDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usernameTextView = findViewById(R.id.loginUsername);
        passwordTextView = findViewById(R.id.loginPassword);
        Button loginBtn = findViewById(R.id.loginBtn);
        Button registerBtn = findViewById(R.id.registerBtn);
        Button unregisterBtn = findViewById(R.id.unregisterBtn);

        unregisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInputs())
                    try {
                        loginDialog = ProgressDialog.show(LoginActivity.this, "Σύνδεση", "Παρακαλώ περιμένετε...", true);
                        String name = usernameTextView.getText().toString().trim();
                        String pass = passwordTextView.getText().toString().trim();
                        Requests.unregisterDevice(name, pass, new Requests.VolleyCallbackUnregister() {
                            @Override
                            public void onSuccess(String response) {
                                System.out.println("Unregister 4 sure");
                                Toast.makeText(LoginActivity.this, "Unregister 4 sure", Toast.LENGTH_SHORT).show();
                                loginDialog.dismiss();
                            }

                            @Override
                            public void onError(VolleyError volleyError) {
                                System.out.println("Error Unregister");
                                Toast.makeText(LoginActivity.this, "Σφαλμα δικτυου. Παρακαλω επικοινωνηστε με την διαχειριση", Toast.LENGTH_SHORT).show();
                                loginDialog.dismiss();
                            }
                        });
                    } catch (MalformedURLException | UnsupportedEncodingException | URISyntaxException e) {
                        e.printStackTrace();
                    }
            }
        });


        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validateInputs())
                    try {
                        loginDialog = ProgressDialog.show(LoginActivity.this, "Σύνδεση", "Παρακαλώ περιμένετε...", true);
                        String name = usernameTextView.getText().toString().trim();
                        String pass = passwordTextView.getText().toString().trim();
                        Requests.login(name, pass, SystemUtils.getRegisterDto().keyedHash, new Requests.VolleyCallbackLogin() {
                            @Override
                            public void onSuccess(String response) {
                                try {

                                    String token = ParserFactory.getLoginToken(response);

                                    final UserDto userDto = new UserDto();
                                    userDto.username = usernameTextView.getText().toString().trim();
                                    userDto.password = passwordTextView.getText().toString().trim();
                                    userDto.token = token;
                                    SystemUtils.saveUserDto(userDto);

                                    Requests.getLastAcademicPeriod(new Requests.VolleyCallbackAcademicPeriod() {
                                        @Override
                                        public void onSuccess(String response) {
                                            try {
                                                Parameters.academic = ParserFactory.getAcademicObject(response);

                                                Requests.getLoggedStudent(userDto.username, new Requests.VolleyCallbackLoggedStudent() {
                                                    @Override
                                                    public void onSuccess(String response) {
                                                        try {
                                                            Parameters.student = ParserFactory.getStudentObject(response);
                                                            startActivity(new Intent(LoginActivity.this, MainActivity.class));

                                                            Toast.makeText(LoginActivity.this, "Επιτυχής σύνδεση!", Toast.LENGTH_SHORT).show();
                                                            finish();

                                                        } catch (XmlPullParserException | IOException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }

                                                    @Override
                                                    public void onError(VolleyError volleyError) {
                                                        startActivity(new Intent(LoginActivity.this, ErrorActivity.class));
                                                        finish();
                                                    }
                                                });

                                            } catch (XmlPullParserException | IOException | URISyntaxException e) {
                                                e.printStackTrace();
                                                errorLoginAlert();
                                            }
                                        }

                                        @Override
                                        public void onError(VolleyError volleyError) {
                                            startActivity(new Intent(LoginActivity.this, ErrorActivity.class));
                                            finish();
                                        }
                                    });

                                } catch (XmlPullParserException | IOException | URISyntaxException e) {
                                    e.printStackTrace();
                                    errorLoginAlert();
                                }
                            }

                            @Override
                            public void onError(VolleyError volleyError) {
                                startActivity(new Intent(LoginActivity.this, ErrorActivity.class));
                                finish();
                            }
                        });
                    } catch (UnsupportedEncodingException | MalformedURLException | URISyntaxException e) {
                        e.printStackTrace();
                        errorLoginAlert();
                    }
            }
        });


        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

    }


    private boolean validateInputs() {
        usernameTextView.setError(null);
        passwordTextView.setError(null);

        if (TextUtils.isEmpty(usernameTextView.getText().toString().trim())) {
            usernameTextView.setError("Υποχρεωτικό πεδίο");
            return false;
        }

        if (TextUtils.isEmpty(passwordTextView.getText().toString().trim())) {
            passwordTextView.setError("Υποχρεωτικό πεδίο");
            return false;
        }

        RegisterDto registerDto = SystemUtils.getRegisterDto();
        if (registerDto.keyedHash == null || registerDto.keyedHash.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("Ο χρήστης δεν είναι εγγεγραμένος. Παρακαλώ κάντε εγγραφή")
                    .setTitle("Σφάλμα")
                    .setNegativeButton("ΟΚ", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            builder.show();
            return false;
        }
        return true;
    }


    private void errorLoginAlert() {
        loginDialog.dismiss();
        new AlertDialog.Builder(LoginActivity.this)
                .setTitle("Προσοχή")
                .setMessage("Παρουσιάστηκε σφάλμα κατά την είδοσο του χρήστη. Παρακαλώ δοκιμάστε ξανά!")
                .show();
    }
}

