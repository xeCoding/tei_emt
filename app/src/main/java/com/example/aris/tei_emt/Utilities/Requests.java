package com.example.aris.tei_emt.Utilities;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Requests {


    public static void login(final String username, final String password, final String id, final VolleyCallbackLogin volleyCallbackLogin) throws UnsupportedEncodingException, URISyntaxException, MalformedURLException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.student/login-" + username
                + "#" + URLEncoder.encode(password, "UTF-8")
                + "#" + id, "", "");
        URL url = uri.toURL();

        System.out.println("Login");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackLogin.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackLogin.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);

    }

    public static void register(final String username, final String password, final String id, final VolleyCallbackRegister volleyCallbackRegister) throws MalformedURLException, UnsupportedEncodingException, URISyntaxException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.student/register-" + username
                + "#" + URLEncoder.encode(password, "UTF-8")
                + "#" + URLEncoder.encode(id, "UTF-8"), "", "");
        URL url = uri.toURL();

        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackRegister.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackRegister.onError(error);
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                return new HashMap<>();
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public static void unregisterDevice(final String username, final String password, final VolleyCallbackUnregister volleyCallbackUnregister) throws MalformedURLException, UnsupportedEncodingException, URISyntaxException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.student/unregister-" + username
                + "#" + URLEncoder.encode(password, "UTF-8"), "", "");
        URL url = uri.toURL();

        System.out.println("unregisterDevice User");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackUnregister.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackUnregister.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public static void getLessons(String academicPeriod, String am, final VolleyCallbackCourses volleyCallbackCourses) throws URISyntaxException, MalformedURLException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.slsview/" + academicPeriod
                + "#" + am
                + "#" + SystemUtils.getUserDto().token, "", "");
        URL url = uri.toURL();

        System.out.println("Get Lessons");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackCourses.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackCourses.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public static void getLectures(final String academicPeriod, final String departmentCode, final String courseCode, final String teamCode, final VolleyCallbackLectures volleyCallbackLectures) throws UnsupportedEncodingException, URISyntaxException, MalformedURLException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.lectures/" + academicPeriod
                + "#" + departmentCode + "#" + URLEncoder.encode(courseCode, "UTF-8") + "#" + URLEncoder.encode(teamCode, "UTF-8")
                + "#" + SystemUtils.getUserDto().token, "", "");
        URL url = uri.toURL();

        System.out.println("Get Lectures");
        //System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackLectures.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackLectures.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public static void getLecturePresence(final String regSlsId, final String id, final VolleyCallbackLecturePresence volleyCallbackLecturePresence) throws URISyntaxException, MalformedURLException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.presence/"
                + regSlsId + "#" + id + "#" + SystemUtils.getUserDto().token, "", "");
        URL url = uri.toURL();

        System.out.println("Get Lecture Presence");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackLecturePresence.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackLecturePresence.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }

    public static void getLastAcademicPeriod(final VolleyCallbackAcademicPeriod volleyCallbackAcademicPeriod) throws URISyntaxException, MalformedURLException {

        URI uri = new URI("http", "", Parameters.host, 80, "/LabsRestApiWebApp/webresources/model.acadperiod/last-328#" +
                SystemUtils.getUserDto().token, "", "");
        URL url = uri.toURL();

        System.out.println("Academic Period");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackAcademicPeriod.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackAcademicPeriod.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);

    }

    public static void getLoggedStudent(final String username, final VolleyCallbackLoggedStudent volleyCallbackLoggedStudent) throws URISyntaxException, MalformedURLException {

        URI uri = new URI("http","",Parameters.host,80, "/LabsRestApiWebApp/webresources/model.student/loggedInStudent-"+username+"#"+
                SystemUtils.getUserDto().token, "", "");
        URL url = uri.toURL();

        System.out.println("Logged Student");
        System.out.println(url.toString());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url.toString(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        volleyCallbackLoggedStudent.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        volleyCallbackLoggedStudent.onError(error);
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/xml;charset=utf-8");
                headers.put("Accept", "application/xml;charset=utf-8");
                return headers;
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                response.headers.put("Content-Type", "application/xml;charset=utf-8");
                return super.parseNetworkResponse(response);
            }
        };
        int socketTimeout = 30000;//30 seconds - change to what you want
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        RequestQueue queue = Volley.newRequestQueue(AppContext.getContext());
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);

    }


    public interface VolleyCallbackLogin {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackRegister {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackCourses {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackLectures {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackLecturePresence {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackUnregister {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackAcademicPeriod {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

    public interface VolleyCallbackLoggedStudent {
        void onSuccess(String response);

        void onError(VolleyError volleyError);
    }

}
