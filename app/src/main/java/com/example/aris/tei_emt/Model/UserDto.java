package com.example.aris.tei_emt.Model;

import java.io.Serializable;

public class UserDto implements Serializable {

    public String username;
    public String password;
    public String token;

    public UserDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
