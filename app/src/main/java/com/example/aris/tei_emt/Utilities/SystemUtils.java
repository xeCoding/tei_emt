package com.example.aris.tei_emt.Utilities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.aris.tei_emt.Activities.LoginActivity;
import com.example.aris.tei_emt.Model.RegisterDto;
import com.example.aris.tei_emt.Model.UserDto;

public class SystemUtils {

    private static Context appContext = AppContext.getContext();
    private static SharedPreferences settings = appContext.getSharedPreferences(Parameters.PREFS_NAME, 0);

    public static String _PseudoIdCreator() {
        return "" + Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                Build.DEVICE.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.TYPE.length() % 10;
    }

    public static void logoutUser() {
        saveUserDto(new UserDto());
        Intent intent = new Intent(AppContext.getContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        AppContext.getContext().startActivity(intent);
    }

    public static void unregisterDevice() {
        UserDto userDto = getUserDto();
        try {
            //ToDo get actual params from Parameters.user
            Requests.unregisterDevice(userDto.username, userDto.password, new Requests.VolleyCallbackUnregister() {
                @Override
                public void onSuccess(String response) {
                    SystemUtils.resetParameters();
                }

                @Override
                public void onError(VolleyError volleyError) {
                    Toast.makeText(appContext, "Πρόβλημα σύνδεσης. Παρακαλώ δοκιμάστε αργότερα!", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(AppContext.getContext(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        AppContext.getContext().startActivity(intent);
    }

    private static void resetParameters() {
        saveUserDto(new UserDto());
        saveRegisterDto(new RegisterDto());
    }

    public static void saveRegisterDto(RegisterDto registerDto) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("deviceId", registerDto.deviceId);
        editor.putString("keyedHash", registerDto.keyedHash);
        editor.apply();
    }

    public static RegisterDto getRegisterDto() {
        RegisterDto registerDto = new RegisterDto();
        registerDto.setDeviceId(settings.getString("deviceId", null));
        registerDto.setKeyedHash(settings.getString("keyedHash", null));

        return registerDto;
    }

    public static void saveUserDto(UserDto userDto) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("username", userDto.username);
        editor.putString("password", userDto.password);
        editor.putString("token", userDto.token);
        editor.apply();
    }

    public static UserDto getUserDto() {
        UserDto userDto = new UserDto();
        userDto.setUsername(settings.getString("username", null));
        userDto.setPassword(settings.getString("password", null));
        userDto.setToken(settings.getString("token", null));

        return userDto;
    }

}
