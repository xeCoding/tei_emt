package com.example.aris.tei_emt.Utilities;

import com.example.aris.tei_emt.Model.AcademicDto;
import com.example.aris.tei_emt.Model.LectureDto;
import com.example.aris.tei_emt.Model.LessonDto;
import com.example.aris.tei_emt.Model.PresenceDto;
import com.example.aris.tei_emt.Model.RegisterDto;
import com.example.aris.tei_emt.Model.StudentDto;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ParserFactory {

    public ParserFactory(InputStream inputStream) throws XmlPullParserException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(inputStream, null);
    }

    public static String getLoginToken(String input) throws XmlPullParserException, IOException {

        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        String token = null;
        int eventType = xmlPullParser.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName = null;

            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("token"))
                    token = xmlPullParser.nextText();
            }
            eventType = xmlPullParser.next();
        }
        return token;
    }

    public static List<LessonDto> getLessonSections(String input) throws XmlPullParserException, IOException {

        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        List<LessonDto> lessonDtos = new ArrayList<>();
        LessonDto lessonDto = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;


            switch (eventType) {
                case XmlPullParser.START_TAG:
                    elementName = xmlPullParser.getName();

                    if (elementName.equals("LessonSections")) {
                        lessonDto = new LessonDto();
                        lessonDtos.add(lessonDto);

                    } else if (lessonDto != null) {
                        switch (elementName) {
                            case "acadid":
                                lessonDto.setAcadId(xmlPullParser.nextText());
                                break;
                            case "aem":
                                lessonDto.setAm(xmlPullParser.nextText());
                                break;
                            case "ayear":
                                lessonDto.setYear(xmlPullParser.nextText());
                                break;
                            case "code":
                                lessonDto.setCode(xmlPullParser.nextText());
                                break;
                            case "dname":
                                lessonDto.setDepartmentName(xmlPullParser.nextText());
                                break;
                            case "dno":
                                lessonDto.setDepartmentNo(xmlPullParser.nextText());
                                break;
                            case "fname":
                                lessonDto.setFname(xmlPullParser.nextText());
                                break;
                            case "lname":
                                lessonDto.setLname(xmlPullParser.nextText());
                                break;
                            case "fulltime":
                                lessonDto.setFullTime(xmlPullParser.nextText());
                                break;
                            case "wdname":
                                lessonDto.setWdname(xmlPullParser.nextText());
                                break;
                            case "regSlsId":
                                lessonDto.setRegSlsID(xmlPullParser.nextText());
                                break;
                            case "semester":
                                lessonDto.setSemester(xmlPullParser.nextText());
                                break;
                            case "lsno":
                                lessonDto.setLsNo(xmlPullParser.nextText());
                                break;
                            case "title":
                                lessonDto.setTitle(xmlPullParser.nextText());
                                break;
                        }
                    }
            }

            eventType = xmlPullParser.next();
        }

        return lessonDtos;
    }

    public static List<LectureDto> getLectureSections(String input) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        List<LectureDto> lectureDtos = new ArrayList<>();
        LectureDto lectureDto = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;

            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("LessonSections")) {
                    lectureDto = new LectureDto();
                    lectureDtos.add(lectureDto);

                } else if (lectureDto != null) {
                    switch (elementName) {
                        case "id":
                            lectureDto.setId(xmlPullParser.nextText());
                            break;
                        case "lecturedate":
                            lectureDto.setLectureDate(xmlPullParser.nextText());
                            break;
                        case "description":
                            lectureDto.setDescription(xmlPullParser.nextText());
                            break;
                        case "deptno":
                            lectureDto.setDeptNo(xmlPullParser.nextText());
                            break;
                    }
                }
            }

            eventType = xmlPullParser.next();
        }

        return lectureDtos;
    }

    public static PresenceDto getLecturePresence(String input) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        PresenceDto presenceDto = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;


            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("PresenceSection")) {
                    presenceDto = new PresenceDto();

                } else if (presenceDto != null) {
                    switch (elementName) {
                        case "presence":
                            presenceDto.setPresence(xmlPullParser.nextText());
                            break;
                        case "lectureId":
                            presenceDto.setLectureId(xmlPullParser.nextText());
                            break;
                        case "regSlsId":
                            presenceDto.setRegSlsId(xmlPullParser.nextText());
                            break;
                    }
                }
            }

            eventType = xmlPullParser.next();
        }

        return presenceDto;
    }

    public static RegisterDto getRegisterObject(String input) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        RegisterDto registerDto = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;


            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("registeredDevice")) {
                    registerDto = new RegisterDto();

                } else if (registerDto != null) {
                    switch (elementName) {
                        case "deviceid":
                            registerDto.setDeviceId(xmlPullParser.nextText());
                            break;
                        case "keyedHash":
                            registerDto.setKeyedHash(xmlPullParser.nextText());
                            break;
                    }
                }
            }

            eventType = xmlPullParser.next();
        }

        return registerDto;
    }

    public static StudentDto getStudentObject(String input) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        StudentDto studentDto= null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;


            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("student")) {
                    studentDto = new StudentDto();

                } else if (studentDto != null) {
                    switch (elementName) {
                        case "email":
                            studentDto.setEmail(xmlPullParser.nextText());
                            break;
                        case "fname":
                            studentDto.setFname(xmlPullParser.nextText());
                            break;
                        case "lname":
                            studentDto.setLname(xmlPullParser.nextText());
                            break;
                        case "semester":
                            studentDto.setSemester(xmlPullParser.nextText());
                            break;
                        case "aem":
                            studentDto.setAem(xmlPullParser.nextText());
                            break;

                    }
                }
            }

            eventType = xmlPullParser.next();
        }

        return studentDto;
    }

    public static AcademicDto getAcademicObject(String input) throws XmlPullParserException, IOException {
        XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlPullParserFactory.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        InputStream inputStream = new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8));
        xmlPullParser.setInput(inputStream, null);

        int eventType = xmlPullParser.getEventType();
        AcademicDto academicDto = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {
            String elementName;


            if (eventType == XmlPullParser.START_TAG) {
                elementName = xmlPullParser.getName();

                if (elementName.equals("acadPeriod")) {
                    academicDto = new AcademicDto();

                } else if (academicDto != null) {
                    switch (elementName) {
                        case "id":
                            academicDto.setId(xmlPullParser.nextText());
                            break;
                        case "ayear":
                            academicDto.setYear(xmlPullParser.nextText());
                            break;
                        case "term":
                            academicDto.setTerm(xmlPullParser.nextText());
                            break;
                        case "regStartDate":
                            academicDto.setRegStartDate(xmlPullParser.nextText());
                            break;
                        case "regEndDate":
                            academicDto.setRegEndDate(xmlPullParser.nextText());
                            break;


                    }
                }
            }

            eventType = xmlPullParser.next();
        }

        return academicDto;
    }

}
