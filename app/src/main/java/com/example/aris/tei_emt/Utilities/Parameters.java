package com.example.aris.tei_emt.Utilities;

import com.example.aris.tei_emt.Model.AcademicDto;
import com.example.aris.tei_emt.Model.StudentDto;

public class Parameters {

    static final String PREFS_NAME = "MySharedPreferences";
    public static AcademicDto academic = null;
    public static StudentDto student = null;

    public static final String host = "labs.teiemt.gr";
}
