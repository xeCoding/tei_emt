package com.example.aris.tei_emt.Adapters.LectureRecycler;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.aris.tei_emt.R;

public class LectureViewHolder extends RecyclerView.ViewHolder {

    TextView date;
    TextView description;
    TextView status;
    ImageButton nfcPresence;
    View group;

    public LectureViewHolder(View itemView) {
        super(itemView);

        group = itemView.findViewById(R.id.group);
        date = itemView.findViewById(R.id.lectureDate);
        description = itemView.findViewById(R.id.lectureDescription);
        status = itemView.findViewById(R.id.lectureStatus);
        nfcPresence = itemView.findViewById(R.id.imageButton);

    }
}
