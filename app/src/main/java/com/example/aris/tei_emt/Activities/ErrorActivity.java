package com.example.aris.tei_emt.Activities;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.SystemUtils;

public class ErrorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);

        Button exit = findViewById(R.id.exitBtn);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SystemUtils.unregisterDevice();
            }
        });

        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Αποτυχία");
    }
}
