package com.example.aris.tei_emt.Model;

import java.io.Serializable;

public class PresenceDto implements Serializable {

    public String presence;
    public String lectureId;
    public String regSlsId;

    public PresenceDto() {
    }

    public String getPresence() {
        return presence;
    }

    public void setPresence(String presence) {
        this.presence = presence;
    }

    public String getLectureId() {
        return lectureId;
    }

    public void setLectureId(String lectureId) {
        this.lectureId = lectureId;
    }

    public String getRegSlsId() {
        return regSlsId;
    }

    public void setRegSlsId(String regSlsId) {
        this.regSlsId = regSlsId;
    }
}
