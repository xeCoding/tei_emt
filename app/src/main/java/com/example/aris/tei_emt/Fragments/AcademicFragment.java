package com.example.aris.tei_emt.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.aris.tei_emt.Model.AcademicDto;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.Parameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AcademicFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AcademicFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AcademicFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AcademicFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AcademicFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AcademicFragment newInstance(String param1, String param2) {
        AcademicFragment fragment = new AcademicFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_academic, container, false);

        final TextView year = view.findViewById(R.id.acadYear);
        final TextView period = view.findViewById(R.id.acadPeriod);
        final TextView regStart = view.findViewById(R.id.acadStatementStart);
        final TextView regEnd = view.findViewById(R.id.acadStatementEnd);

        AcademicDto academicDto = Parameters.academic;

        year.setText(academicDto.year);
        period.setText(academicDto.term);

        try {
            Date dateStart = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", new Locale("el", "GR")).parse(academicDto.regStartDate);
            String dateStringStart = new SimpleDateFormat("dd MMM yyyy", new Locale("el", "GR")).format(dateStart);
            regStart.setText(dateStringStart);
        } catch (ParseException e) {
            e.printStackTrace();
            regStart.setText("Μη διαθέσιμη ημ/νία");
        }

        try {
            Date dateEnd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", new Locale("el", "GR")).parse(academicDto.regEndDate);
            String dateStringEnd = new SimpleDateFormat("dd MMM yyyy", new Locale("el", "GR")).format(dateEnd);
            regEnd.setText(dateStringEnd);
        } catch (ParseException e) {
            e.printStackTrace();
            regEnd.setText("Μη διαθέσιμη ημ/νία");
        }

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
