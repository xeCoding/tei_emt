package com.example.aris.tei_emt.Model;

import java.io.Serializable;

public class LessonDto implements Serializable {

    public String acadId;
    public String am;
    public String year;
    public String code;
    public String departmentName;
    public String departmentNo;
    public String fname;
    public String lname;
    public String fullTime;
    public String regSlsID;
    public String lsNo;
    public String title;
    public String wdname;
    public String semester;


    public LessonDto() {
    }

    public String getAm() {
        return am;
    }

    public void setAm(String am) {
        this.am = am;
    }

    public String getAcadId() {
        return acadId;
    }

    public void setAcadId(String acadId) {
        this.acadId = acadId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentNo() {
        return departmentNo;
    }

    public void setDepartmentNo(String departmentNo) {
        this.departmentNo = departmentNo;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFullTime() {
        return fullTime;
    }

    public void setFullTime(String fullTime) {
        this.fullTime = fullTime;
    }

    public String getRegSlsID() {
        return regSlsID;
    }

    public void setRegSlsID(String regSlsID) {
        this.regSlsID = regSlsID;
    }

    public String getLsNo() {
        return lsNo;
    }

    public void setLsNo(String lsNo) {
        this.lsNo = lsNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWdname() {
        return wdname;
    }

    public void setWdname(String wdname) {
        this.wdname = wdname;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }
}
