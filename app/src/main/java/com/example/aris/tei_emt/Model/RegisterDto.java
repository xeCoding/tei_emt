package com.example.aris.tei_emt.Model;

import java.io.Serializable;

public class RegisterDto implements Serializable {

    public String deviceId;
    public String keyedHash;

    public RegisterDto() {
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getKeyedHash() {
        return keyedHash;
    }

    public void setKeyedHash(String keyedHash) {
        this.keyedHash = keyedHash;
    }
}
