package com.example.aris.tei_emt.Activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.aris.tei_emt.Model.RegisterDto;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.ParserFactory;
import com.example.aris.tei_emt.Utilities.Requests;
import com.example.aris.tei_emt.Utilities.SystemUtils;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

public class RegisterActivity extends AppCompatActivity {

    private EditText usernameTextView;
    private EditText passwordTextView;
    private ProgressDialog registerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        usernameTextView = findViewById(R.id.loginUsername);
        passwordTextView = findViewById(R.id.loginPassword);
        final Button registerBtn = findViewById(R.id.registerBtn);

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateInputs())
                    try {
                        registerDialog = ProgressDialog.show(RegisterActivity.this, "Εγγραφή", "Παρακαλώ περιμένετε...", true);
                        String name = usernameTextView.getText().toString().trim();
                        String pass = passwordTextView.getText().toString().trim();
                        Requests.register(name, pass, SystemUtils._PseudoIdCreator(), new Requests.VolleyCallbackRegister() {
                            @Override
                            public void onSuccess(String response) {
                                try {
                                    RegisterDto registerDto = ParserFactory.getRegisterObject(response);
                                    SystemUtils.saveRegisterDto(registerDto);
                                    Toast.makeText(RegisterActivity.this, "Εγγραφή επιτυχής!", Toast.LENGTH_SHORT).show();
                                    finish();
                                } catch (XmlPullParserException | IOException e) {
                                    e.printStackTrace();
                                    startActivity(new Intent(RegisterActivity.this, ErrorActivity.class));
                                    finish();
                                }
                            }

                            @Override
                            public void onError(VolleyError volleyError) {
                                registerDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                                builder.setMessage("Δεν μπόρεσε να γίνει εγγραφή συσκευής. Παρακαλώ επικοινωνήστε με τη διαχείρηση")
                                        .setTitle("Σφάλμα")
                                        .setNegativeButton("ΟΚ", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                            }
                                        });
                                builder.show();
                            }
                        });
                    } catch (MalformedURLException | UnsupportedEncodingException | URISyntaxException e) {
                        e.printStackTrace();
                        startActivity(new Intent(RegisterActivity.this, ErrorActivity.class));
                        finish();
                    }
            }
        });
    }

    private boolean validateInputs() {
        usernameTextView.setError(null);
        passwordTextView.setError(null);

        if (TextUtils.isEmpty(usernameTextView.getText().toString().trim())) {
            usernameTextView.setError("Υποχρεωτικό πεδίο");
            return false;
        }

        if (TextUtils.isEmpty(passwordTextView.getText().toString().trim())) {
            passwordTextView.setError("Υποχρεωτικό πεδίο");
            return false;
        }
        return true;
    }
}
