package com.example.aris.tei_emt.Adapters.LectureRecycler;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.example.aris.tei_emt.Activities.NfcActivity;
import com.example.aris.tei_emt.Model.LectureDto;
import com.example.aris.tei_emt.Model.LessonDto;
import com.example.aris.tei_emt.Model.PresenceDto;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.ParserFactory;
import com.example.aris.tei_emt.Utilities.Requests;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RecyclerAdapter extends RecyclerView.Adapter<LectureViewHolder> {

    private Context activityContext;
    private List<LectureDto> data;
    private LessonDto lessonDto;

    public RecyclerAdapter(Context context, List<LectureDto> dataList, LessonDto lessonDto) {
        this.activityContext = context;
        this.data = new ArrayList<>();
        this.data.addAll(dataList);
        this.lessonDto = lessonDto;
    }

    @NonNull
    @Override
    public LectureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activityContext)
                .inflate(R.layout.lecture_item, parent, false);

        return new LectureViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final LectureViewHolder holder, int position) {

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", new Locale("el", "GR")).parse(data.get(position).lectureDate);
            String dateString = new SimpleDateFormat("dd MMM yyyy", new Locale("el", "GR")).format(date);
            holder.date.setText(dateString);
        } catch (Exception e) {
            holder.date.setText("Μη διαθέσιμη ημ/νία");
            e.printStackTrace();
        }

        holder.description.setText(data.get(position).description);

        try {
            Requests.getLecturePresence(lessonDto.regSlsID, data.get(position).id, new Requests.VolleyCallbackLecturePresence() {
                @Override
                public void onSuccess(String response) {

                    try {
                        PresenceDto presenceDto = ParserFactory.getLecturePresence(response);
                        if (presenceDto != null) {
                            if (presenceDto.presence.equals("0")) {
                                holder.status.setText("Απουσία");
                                holder.status.setTextColor(activityContext.getResources().getColor(R.color.colorAbsent));
                                holder.group.setVisibility(View.VISIBLE);
                            } else {
                                holder.status.setText("Παρουσία");
                                holder.status.setTextColor(activityContext.getResources().getColor(R.color.colorPresent));
                            }
                        } else {
                            holder.status.setText("N/A");
                            holder.status.setTextColor(activityContext.getResources().getColor(R.color.colorAbsent));
                        }

                    } catch (XmlPullParserException | IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(VolleyError volleyError) {
                    holder.status.setText("N/A");
                    holder.status.setTextColor(activityContext.getResources().getColor(R.color.colorAbsent));
                }
            });
        } catch (URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        }

        holder.nfcPresence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activityContext, "Nfc pressed", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activityContext, NfcActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activityContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
