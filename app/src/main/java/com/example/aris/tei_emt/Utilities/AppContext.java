package com.example.aris.tei_emt.Utilities;


import android.app.Application;
import android.content.Context;

public class AppContext extends Application {

    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }
}