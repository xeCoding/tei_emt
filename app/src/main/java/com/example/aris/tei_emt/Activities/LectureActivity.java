package com.example.aris.tei_emt.Activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;

import com.android.volley.VolleyError;
import com.example.aris.tei_emt.Adapters.LectureRecycler.RecyclerAdapter;
import com.example.aris.tei_emt.Model.LectureDto;
import com.example.aris.tei_emt.Model.LessonDto;
import com.example.aris.tei_emt.R;
import com.example.aris.tei_emt.Utilities.AppContext;
import com.example.aris.tei_emt.Utilities.ParserFactory;
import com.example.aris.tei_emt.Utilities.Requests;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.List;

public class LectureActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecture);

        if (getSupportActionBar() != null) {
            setTitle("Διαλέξεις");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        recyclerView = findViewById(R.id.lectureRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(AppContext.getContext()));


        // ToDo Handle Null getIntent.getExtras
        if (getIntent().getExtras() == null) {
            startActivity(new Intent(LectureActivity.this, ErrorActivity.class));
            finish();
        }

        final LessonDto lessonDto = (LessonDto) getIntent().getExtras().getSerializable("lectureSectionDto");

        if (lessonDto == null)
            startActivity(new Intent(LectureActivity.this, LoginActivity.class));


        try {
            Requests.getLectures(lessonDto.acadId, lessonDto.departmentNo, lessonDto.code, lessonDto.lsNo, new Requests.VolleyCallbackLectures() {
                @Override
                public void onSuccess(String response) {
                    try {
                        List<LectureDto> lectureDtoList = ParserFactory.getLectureSections(response);
                        RecyclerAdapter recyclerAdapter = new RecyclerAdapter(LectureActivity.this, lectureDtoList, lessonDto);
                        recyclerView.setAdapter(recyclerAdapter);
                    } catch (XmlPullParserException | IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(VolleyError volleyError) {

                }
            });
        } catch (UnsupportedEncodingException | URISyntaxException | MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
