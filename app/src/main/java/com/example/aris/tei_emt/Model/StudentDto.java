package com.example.aris.tei_emt.Model;

import java.io.Serializable;

public class StudentDto implements Serializable {

    public String lname;
    public String fname;
    public String semester;
    public String email;
    public String aem;
    public String deptno;


    public StudentDto() {
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDeptno() {
        return deptno;
    }

    public void setDeptno(String deptno) {
        this.deptno = deptno;
    }

    public String getAem() {
        return aem;
    }

    public void setAem(String aem) {
        this.aem = aem;
    }
}
